const { src, dest, parallel } = require("gulp")
const concat = require("gulp-concat")
const extReplace = require("gulp-ext-replace")
const postcss = require("gulp-postcss")

const plugins = {
  autoprefixer: require("autoprefixer")({}),
  nested: require("postcss-nested"),
  stylelint: require("stylelint"),
  discardComments: require("postcss-discard-comments"),
  mergeRules: require("postcss-merge-rules"),
  minifySelectors: require("postcss-minify-selectors"),
  normalizeWhitespace: require("postcss-normalize-whitespace")
}

const theme = (name) => {
  return src(`src/${name}.pcss`)
    .pipe(postcss([
      plugins.autoprefixer,
      plugins.nested,
      plugins.stylelint
    ]))
    .pipe(extReplace('.css'))
    .pipe(dest("dist/"))
}

const themes = {
  dark: () => theme("dark-theme"),
  light: () => theme("light-theme"),
}

const normalize = () => {
  return src("src/{normalize,fallback}.pcss")
    .pipe(postcss([
      plugins.autoprefixer,
      plugins.nested,
      plugins.discardComments,
      plugins.mergeRules,
      plugins.minifySelectors,
      plugins.normalizeWhitespace
    ]))
    .pipe(concat('normalize.css'))
    .pipe(dest("dist/"))
}

exports.build = parallel(normalize, ...Object.values(themes))
